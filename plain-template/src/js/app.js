(function(e) {
    e(window.jQuery, window, document);
})(function($, window, document) {
    console.log('init');
    var app = {

        // ==============================================================================================
        // Call your function here to become a single function
        // * This method make your code more modular and make it easy to toggle your function
        // * If you want to disable a function, just commented on function that you need to disable below
        // ==============================================================================================

        init: function($) {
            app.slideToggle(),
            app.validateLogin(),
            app.validateRegistStartUp();
        },

        // ======================================================================
        // Your function here
        // * Don't forget to use proper function name to describes your function
        // ======================================================================
        slideToggle: function(){
          $(".link-toggle-down").click(function(){
            var parent = $(this).parent();
            var down = parent.find($(".slide-toggle-item"));
            var span = $(this).find($(".span-down"));
            down.slideToggle();
            if(span.hasClass("animate")) {
              // console.log("remove");
              span.removeClass("animate");
            } else {
              // console.log("add");
              span.addClass("animate");
            }
          })
        },
        validateLogin: function(){
          $("#loginForm").validate({
            rules: {
              email: {
                required: true,
                email: true
              },
              password: {
                minlength: 6,
                required: true,
                password: true
              }
            }
          })
        },
        validateRegistStartUp: function(){
          $("#registStartUpForm").validate({
            rules: {
              suname: {
                required: true
              },
              sudesc: {
                required: true
              },
              suvision: {
                required: true
              },
              sumission: {
                required: true
              },
              surp: {
                required: true
              },
              suclass: {
                required: true
              },
              sujobdesc: {
                required: true
              },
              sufile: {
                required: true
              },
            }
          })
        },
    }
    $(document).ready(function () {
        app.init($);
    });

});
