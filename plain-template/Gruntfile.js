module.exports = function(grunt){
  grunt.initConfig({
    sass: {
      options: {
        style: 'compressed'
      },
      dist: {
        files: {
          'build/css/style.min.css': ['src/scss/main.scss']
        }
      }
    },
    uglify: {
      build: {
        files: {
          'build/js/app.min.js': ['src/js/*.js']
        }
      }
    },
    codekit: {
      files: {
        src: ['**/*.kit'],
        dest: 'build'
      }
    },
    watch: {
      scss: {
        files: ['src/scss/*.scss'],
        tasks: ['sass']
      },
      uglify: {
        files: ['src/js/*.js'],
        tasks: ['uglify']
      },
      codekit: {
        files: ['**/*.kit'],
        tasks: ['codekit']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
  require('load-grunt-tasks')(grunt);
}
