<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="robots" content="index,follow">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>Avessor</title>

  <!-- Shortcut icon -->
	<link rel="shortcut icon" type="x-icon" href="./assets/img/icon.png">

  <!-- CSS HERE -->
  <link rel="stylesheet" href="./vendor/normalize.css">
  <link rel="stylesheet" href="./vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="./vendor/fontawesome/css/all.min.css">
	<link rel="stylesheet" href="./css/style.min.css">

</head>

<body>
  <section class="container-fluid">
  <section class="row">
    <div class="col-md-10 offset-md-1">
      <nav class="navbar navbar-expand-lg navbar-light py-3">
        <a class="navbar-brand" href="#">
          <img src="./img/logo.png" class="d-inline-block align-top" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Daftar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#loginModal">Masuk</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </section>
</section>
<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body m-3">
        <div class="d-flex justify-content-end">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="text-center">
          <h5 class="modal-title mt-2 mb-4" id="loginModalLabel">Masuk di Avessor</h5>
        </div>
        <form class="" id="loginForm" action="index.html" method="post">
          <input class="form-control mt-3 my-1" name="email" type="email" placeholder="Email">
          <input class="form-control mt-3 my-1" name="password" type="password" placeholder="Password">
          <div class="d-flex justify-content-center pt-3">
            <button type="submit" class="btn btn-primary" name="button">Masuk</button>
          </div>
        </form>
        <p class="my-3 text-center">atau</p>
        <div class="d-flex justify-content-between">
          <button type="button" class="btn btn-danger" name="button"><i class="fab fa-google-plus-g pr-2"></i> Google</button>
          <button type="button" class="btn btn-primary" name="button"><i class="fab fa-facebook-f pr-2"></i> Facebook</button>
        </div>
        <p class="text-center mb-0 mt-3">Belum mempunyai akun? <a href="#">Daftar</a></p>
      </div>
    </div>
  </div>
</div>
<!-- /Modal -->

  <section style="background-image: url('./img/banner.jpg')" class="wrap-banner">
  <h2>Selamat Datang di Avessor</h2>
  <section class="container-fluid">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form class="bind">
          <section class="">
            <div class="">
              <i class="fa fa-lg fa-search"></i>
              <input type="text" class="form-control" placeholder="Nama Startup/Kategori/Keahlian">
            </div>
            <div class="">
              <i class="fas fa-lg fa-map-marker-alt"></i>
              <input type="text" class="form-control" placeholder="Lokasi">
            </div>
            <div class="">
              <button type="submit" class="btn btn-block btn-primary mb-2">Cari</button>
            </div>
          </section>
        </form>
      </div>
    </div>
  </section>
</section>

  <section class="container-fluid index">
  <section class="row">
    <div class="col-md-10 offset-md-1">
      <h3>Tentang Kami</h3>
      <hr class="hr-ave">
      <section class="row text-section">
        <div class="col-md-7">
          <p class="">Platform Penghubung antara Pemilik Ide Inovasi dengan Tenaga Ahli untuk Tim Startup. Dan Media Pengembangan Sumber Daya Manusia dengan Menghadirkan Kursus Online dari Profesional</p>
        </div>
        <div class="col-md-3 offset-md-2">
          <figure>
            <img src="./img/12.jpg" alt="">
          </figure>
        </div>
      </section>
    </div>
  </section>
</section>
<section class="container-fluid index">
  <section class="row">
    <div class="col-md-10 offset-md-1">
      <h3>Cara Kerja Kami</h3>
      <hr class="hr-ave">
      <section class="row thumb-section">
        <div class="col-md-4">
          <a href="#">
            <figure>
              <img src="./img/pass.png" alt="">
              <p>Buat Identitas Profesionalmu</p>
            </figure>
          </a>
        </div>
        <div class="col-md-4">
          <a href="#">
            <figure>
              <img src="./img/connection.png" alt="">
              <p>Cari dan Bentuk Timmu</p>
            </figure>
          </a>
        </div>
        <div class="col-md-4">
          <a href="#">
            <figure>
              <img src="./img/project-management.png" alt="">
              <p>Bangun Startupmu</p>
            </figure>
          </a>
        </div>
      </section>
    </div>
  </section>
</section>

  <footer>
  <section class="container-fluid">
    <section class="row">
      <div class="col-md-10 offset-md-1">
        <section class="d-flex foot-section">
          <div class="d-flex">
            <a href="#">Tentang Kami</a>
            <a href="#">Startup</a>
            <a href="#">Ketentuan Layanan</a>
          </div>
          <div class="d-flex">
            <a href="#"><i class="fab fa-lg fa-instagram"></i></a>
            <a href="#"><i class="fab fa-lg fa-line"></i></a>
          </div>
        </section>
        <p class="text-center text-secondary mt-b mt-2">Avessor &copy 2019</p>
      </div>
    </section>
  </section>
</footer>

<!-- JAVASCRIPT HERE -->
<script type="text/javascript" src="./js/jquery.min.js"></script>
<script type="text/javascript" src="./js/jquery.validate.min.js"></script>
<script type="text/javascript" src="./vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/app.min.js"></script>
</body>
</html>

